variable "project_id" {
    description = "the AMI to use"
}
variable "secret" {
    description = "the secret.json to get access to GCP project"
}
variable "image_name" {
    description = "the name of the image to use"
}

provider "google" {
    credentials = var.secret
    project = var.project_id
    region = "us-central1"
}

resource "google_compute_instance" "default" {
    name = var.image_name 
    machine_type = "n1-standard-1"
    zone= "us-central1-a"

    boot_disk {
        initialize_params {
	    image = var.image_name
        }
    }
    network_interface {
        network = "default"
        access_config {
            // Ephemeral IP
        }
    }
}
